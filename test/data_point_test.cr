require "minitest/autorun"

require "/../src/data_point.cr"

class DataPointTest < Minitest::Test
  def test_parses_data_point
    json = %({"name": "hospitalizations","region": "ON","date": "2022-10-06","value": 1456,"value_daily": 65})

    data_point = OpenCovid::DataPoint.from_json(json)

    assert_equal "hospitalizations", data_point.name
    assert_equal "ON", data_point.region
    assert_equal Time.utc(2022, 10, 6), data_point.date
    assert_equal 1456, data_point.value
    assert_equal 65, data_point.value_daily
  end
end
