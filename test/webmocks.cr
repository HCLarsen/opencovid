require "webmock"

WebMock.stub(:get, "https://api.opencovid.ca/timeseries?stat=cases&stat=deaths&stat=hospitalizations&geo=pt&loc=on").to_return(status: 200, body: File.read("test/files/ontario-timeseries.json"))

WebMock.stub(:get, "https://api.opencovid.ca/timeseries?stat=cases&stat=deaths&stat=hospitalizations&geo=can").to_return(status: 200, body: File.read("test/files/canada-timeseries.json"))
