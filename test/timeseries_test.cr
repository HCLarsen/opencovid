require "minitest/autorun"

require "/../src/timeseries.cr"

class DataPointTest < Minitest::Test
  def test_parses_timeseries
    json = File.read("test/files/canada-timeseries.json")

    timeseries = OpenCovid::Timeseries.from_json(json, root: "data")

    cases = timeseries.cases
    first = cases.first
    last = cases.last

    assert_equal "cases", first.name
    assert_equal "CAN", first.region

    assert_equal Time.utc(2020, 1, 24), first.date
    assert_equal 0, first.value
    assert_equal 0, first.value_daily

    assert_equal Time.utc(2022, 10, 7), last.date
    assert_equal 4267311, last.value
    assert_equal 2, last.value_daily

    deaths = timeseries.deaths
    first = deaths.first
    last = deaths.last

    assert_equal "deaths", first.name
    assert_equal "CAN", first.region

    assert_equal Time.utc(2020, 1, 24), first.date
    assert_equal 0, first.value
    assert_equal 0, first.value_daily

    assert_equal Time.utc(2022, 10, 5), last.date
    assert_equal 45163, last.value
    assert_equal 27, last.value_daily

    hospitalizations = timeseries.hospitalizations
    first = hospitalizations.first
    last = hospitalizations.last

    assert_equal "hospitalizations", first.name
    assert_equal "CAN", first.region

    assert_equal Time.utc(2020, 1, 25), first.date
    assert_equal 0, first.value
    assert_equal 0, first.value_daily

    assert_equal Time.utc(2022, 10, 7), last.date
    assert_equal 5148, last.value
    assert_equal 19, last.value_daily
  end
end
