require "minitest/autorun"

require "/../src/opencovid.cr"
require "./webmocks.cr"

class OpenCovidTest < Minitest::Test
  def test_gets_canada_timeseries
    timeseries = OpenCovid.get_canada_timeseries

    cases = timeseries.cases
    last = cases.last

    assert_equal "cases", last.name
    assert_equal "CAN", last.region
    assert_equal Time.utc(2022, 10, 7), last.date
    assert_equal 4267311, last.value
    assert_equal 2, last.value_daily

    deaths = timeseries.deaths
    assert_equal "deaths", deaths.last.name

    hospitalizations = timeseries.hospitalizations
    assert_equal "hospitalizations", hospitalizations.last.name
  end

  def test_gets_ontario_timeseries
    timeseries = OpenCovid.get_province_timeseries("ON")

    cases = timeseries.cases
    last = cases.last

    assert_equal "cases", last.name
    assert_equal "ON", last.region
    assert_equal Time.utc(2022, 10, 5), last.date
    assert_equal 1457621, last.value
    assert_equal 1631, last.value_daily

    deaths = timeseries.deaths
    assert_equal "deaths", deaths.last.name

    hospitalizations = timeseries.hospitalizations
    assert_equal "hospitalizations", hospitalizations.last.name
  end
end
