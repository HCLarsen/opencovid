require "./data_point"

module OpenCovid
  struct Timeseries
    include JSON::Serializable

    getter cases : Array(DataPoint)
    getter deaths : Array(DataPoint)
    getter hospitalizations : Array(DataPoint)
  end
end
