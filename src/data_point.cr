require "json"

module OpenCovid
  struct DataPoint
    include JSON::Serializable

    getter name : String
    getter region : String
    @[JSON::Field(converter: Time::Format.new("%Y-%m-%d"))]
    getter date : Time
    getter value : Int32
    getter value_daily : Int32
  end
end
