require "./timeseries"

# TODO: Write documentation for `OpenCovid`
module OpenCovid
  VERSION = "0.1.0"

  def self.get_canada_timeseries : Timeseries
    data = HTTP::Client.get("https://api.opencovid.ca/timeseries?stat=cases&stat=deaths&stat=hospitalizations&geo=can")

    OpenCovid::Timeseries.from_json(data.body, root: "data")
  end

  def self.get_province_timeseries(province_code : String) : Timeseries
    code = province_code.downcase

    data = HTTP::Client.get("https://api.opencovid.ca/timeseries?stat=cases&stat=deaths&stat=hospitalizations&geo=pt&loc=#{code}")

    OpenCovid::Timeseries.from_json(data.body, root: "data")
  end
end
